# This file is part of Health appointment  FIUNER module for GNU Health.
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from . import appointment_diary_report
from . import appointment_report
from . import daily_report

