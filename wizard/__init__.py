# This file is part of Health appointment  FIUNER module for GNU Health.
# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from . import create_diary_appointment_report
from . import wizard_create_appointment_report
from . import wizard_health_calendar
from . import wizard_health_daily_appointment
from . import wizard_appointment_patient


